-- You can add your own plugins here or in other files in this directory!
--  I promise not to create any merge conflicts in this directory :)
--
-- See the kickstart.nvim README for more information
return {
  '3rd/image.nvim',
  config = function()
    require('lazy').setup({
      {
        '3rd/image.nvim',
        opts = {},
      },
    }, {
      rocks = {
        hererocks = true, -- recommended if you do not have global installation of Lua 5.1.
      },
    })
  end,
}
